#ifndef CircBuffer_HPP
#define CircBuffer_HPP

#include <stdint.h>

typedef struct
{
	int head;   //always 1 char ahead buffered string
	int tail;   //always last char of buffered string

	int bufferMaxLength;
	char *buffer;

	int retMaxLength;
    char *ret;

    int newlineCount;   //when non-optimised operation is set, this will always equal to -1
} CircBuffer_HandleTypeDef;

CircBuffer_HandleTypeDef* CircBuffer_init(int size, uint8_t optimiseNewline);
void CircBuffer_deInit(CircBuffer_HandleTypeDef *cbuf);

int CircBuffer_getCount(CircBuffer_HandleTypeDef *cbuf);

char CircBuffer_getCharAt(CircBuffer_HandleTypeDef *cbuf, int pos);
char CircBuffer_getChar(CircBuffer_HandleTypeDef *cbuf);


int CircBuffer_getTokenIndex(CircBuffer_HandleTypeDef *cbuf, char *tokens);
int CircBuffer_getCharIndex(CircBuffer_HandleTypeDef *cbuf, char chr);

char* CircBuffer_getLineTokenized(CircBuffer_HandleTypeDef *cbuf, char *tokens);
char* CircBuffer_getLine(CircBuffer_HandleTypeDef *cbuf);
int CircBuffer_getLineCount(CircBuffer_HandleTypeDef *cbuf);

uint8_t CircBuffer_addChar(CircBuffer_HandleTypeDef *cbuf, char chr);

#endif // CircularBuffer_HPP
