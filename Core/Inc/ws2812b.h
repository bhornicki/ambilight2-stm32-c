/*
 * ws2812b.h
 *
 *	The MIT License.
 *	Created on: 14.07.2017
 *		Author: Mateusz Salamon
 *		www.msalamon.pl
 *		mateusz@msalamon.pl
 */

#ifndef WS2812B_H_
#define WS2812B_H_

// For 6 MHz SPI + DMA

typedef struct Ws2812b_color
{
	uint8_t red, green, blue;
} Ws2812b_color;

void WS2812B_Init(SPI_HandleTypeDef *spi_handle, int led_count);
void WS2812B_SetHspi(SPI_HandleTypeDef *spi_handle);
void WS2812B_DeInit();
void WS2812B_SetDiodeColor(int16_t diode_id, uint32_t color);
void WS2812B_SetDiodeColorStruct(int16_t diode_id, Ws2812b_color color);
void WS2812B_SetDiodeRGB(int16_t diode_id, uint8_t R, uint8_t G, uint8_t B);
void WS2812B_SetDiodeHSV(int16_t diode_id, uint16_t Hue, uint8_t Saturation, uint8_t Brightness);
void WS2812B_SetAllDiodesRGB(uint8_t R, uint8_t G, uint8_t B);
uint32_t WS2812B_GetColor(int16_t diode_id);
uint8_t* WS2812B_GetPixels(void);
int WS2812B_GetLedCount();
void WS2812B_Refresh();
void WS2812B_Resize(int led_count);
extern const uint8_t WS2812B_gammaTable[256];

#endif /* WS2812B_H_ */
