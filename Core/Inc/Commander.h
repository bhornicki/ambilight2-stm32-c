/*
 * UsbParser.h
 *
 *  Created on: 8 gru 2019
 *      Author: HHV
 */

#ifndef SRC_USBPARSER_H_
#define SRC_USBPARSER_H_

#include "stm32f1xx_hal.h"

void Commander_init(SPI_HandleTypeDef *spi_handle);
void Commander_mainLoop();

#endif /* SRC_USBPARSER_H_ */
