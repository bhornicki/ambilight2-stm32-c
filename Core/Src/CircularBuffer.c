#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <stdint.h>

#include "CircularBuffer.h"

#ifndef NULL
	#define NULL 0
#endif

/**
 * @brief CircBuffer_init Allocates circular buffer struct. Note that actual capacitance is size-1, because head is always one char ahead of string
 * @param size How many characters will the buffer maximally store
 * @param optimiseNewline Should buffer convert CRLF to LF and speed up line count detection. 0 = false, other value = true
 * @return Circular buffer struct pointer
 */
CircBuffer_HandleTypeDef* CircBuffer_init(int size, uint8_t optimiseNewline)
{
    if(size<=1)
        return NULL;
    CircBuffer_HandleTypeDef *cret = calloc(1,sizeof(CircBuffer_HandleTypeDef));
    //If allocation failed don't continue
    if(cret==NULL)
        return NULL;

    cret->bufferMaxLength = size;
    cret->buffer = malloc(size);

    if(optimiseNewline==0)
        cret->newlineCount = -1;
    return cret;
}

/**
 * @brief CircBuffer_deInit Deallocates entire circular buffer struct with it sub-arrays
 * @param cbuf Pointer to circular buffer struct
 */
void CircBuffer_deInit(CircBuffer_HandleTypeDef *cbuf)
{
	if (cbuf == NULL)
		return;

    free(cbuf->ret);
	free(cbuf->buffer);

    cbuf->ret=NULL;
    cbuf->buffer=NULL;

    free(cbuf);
}

/**
 * @brief CircBuffer_getCount
 * @param cbuf Pointer to circular buffer struct
 * @return Ammount of chars which haven't been read by CircBuffer_getChar() yet. -1 if cbuf is invalid
 */
int CircBuffer_getCount(CircBuffer_HandleTypeDef *cbuf)
{
    if (cbuf == NULL || cbuf->buffer == NULL)
		return -1;
	return (cbuf->head + cbuf->bufferMaxLength - cbuf->tail) % (cbuf->bufferMaxLength);
}

/**
 * @brief getCharAt Returns character at given position, counting from buffer tail (which points to character from CirclarBuffer_getChar()). Does not change tail.
 * @param pos Index of char
 * @return Character at given position. -1 if cbuf is invalid
 */
char CircBuffer_getCharAt(CircBuffer_HandleTypeDef *cbuf, int pos)
{
	if (cbuf == NULL || cbuf->buffer == NULL)
		return -1;
	return cbuf->buffer[(cbuf->tail + pos) % cbuf->bufferMaxLength];
}

/**
 *
 * @brief CircBuffer_getChar Returns next char from buffer and updates tail
 * @param cbuf Pointer to circular buffer struct
 * @return Next char from buffer. 0 if cbuf is invalid
 */
char CircBuffer_getChar(CircBuffer_HandleTypeDef *cbuf)
{
	if (cbuf == NULL || cbuf->buffer == NULL)
		return 0;
    if (CircBuffer_getCount(cbuf) == 0)
		return 0;

	int temp_tail = cbuf->tail;
	cbuf->tail = (cbuf->tail + 1) % cbuf->bufferMaxLength;

    if(cbuf->newlineCount>0 && cbuf->buffer[temp_tail]=='\n')
    	cbuf->newlineCount--;

	return cbuf->buffer[temp_tail];
}

/**
 * @brief CircBuffer_getTokenIndex Searches buffer for first occurence of ANY characer from tokens string
 * @param cbuf Pointer to circular buffer struct
 * @param tokens String with tokens
 * @return Ammount of characters until first token. -1 if cbuf is invalid or token not found
 */
int CircBuffer_getTokenIndex(CircBuffer_HandleTypeDef *cbuf, char *tokens)
{
	if (tokens == NULL || cbuf == NULL)
		return -1;
	int i;
    int tokenCnt = strlen(tokens);
    for (i = 0; i < CircBuffer_getCount(cbuf); i++)
        for (int j = 0; j < tokenCnt; j++)
        	if (CircBuffer_getCharAt(cbuf, i) == tokens[j])
				return i;
	return -1;
}

/**
 * @brief CircBuffer_getCharIndex Searches buffer for first occurence of character
 * @param cbuf Pointer to circular buffer struct
 * @param chr char to search for
 * @return Index of char starting from 0. If not found or cbuf is invalid, returns -1
 */
int CircBuffer_getCharIndex(CircBuffer_HandleTypeDef *cbuf, char chr)
{
	if (cbuf == NULL)
		return -1;
    for (int i = 0; i < CircBuffer_getCount(cbuf); i++)
		if (CircBuffer_getCharAt(cbuf, i) == chr)
			return i;
	return -1;
}

/**
 * @brief CircBuffer_prepareRetMemory Prepares cbuf->ret to fit entire line which will be returned. If new size is smaller - won't shrink it.
 * @param cbuf Pointer to circular buffer struct
 * @param len New ret string size
 */
void CircBuffer_prepareRetMemory(CircBuffer_HandleTypeDef *cbuf, int len)
{
	//TODO: memory managment. Detect if ret can be shrunken down.
	if (cbuf == NULL)
		return;

	//prepare memory block
	len++;  //null-termination
	if (len > cbuf->retMaxLength)
	{
		cbuf->ret = (char*) realloc(cbuf->ret, len);
		cbuf->retMaxLength = len;
	}

}

/**
 * @brief CircBuffer_getLineTokenized Returns pointer to null-terminated string without token at the end. If tokens equals NULL, will return entire buffer content
 * @param cbuf Pointer to circular buffer struct
 * @param tokens String with tokens or NULL
 * @return Pointer to null-terminated string. NULL if cbuf is invalid. If no token found - returns empty string
 */
char* CircBuffer_getLineTokenized(CircBuffer_HandleTypeDef *cbuf, char *tokens)
{
	if (cbuf == NULL)
		return NULL;
	int len;
	if (tokens)
		len = CircBuffer_getTokenIndex(cbuf, tokens);
	else
        len = CircBuffer_getCount(cbuf);

	if (len == -1)
    {
        CircBuffer_prepareRetMemory(cbuf, 0);
        cbuf->ret[0]='\0';
		return cbuf->ret;
    }

	CircBuffer_prepareRetMemory(cbuf, len);

	//copy text
	for (int i = 0; i < len; i++)
		cbuf->ret[i] = CircBuffer_getChar(cbuf);

	//manage buf ending, deletion of tokens
	cbuf->ret[len] = 0;
	int new_tail = cbuf->tail;
    int tokenCnt = strlen(tokens);

    for (int i = 0; i < CircBuffer_getCount(cbuf); i++)
	{
		uint8_t anyToken = 0;
        for (int j = 0; j < tokenCnt; j++)
			if (CircBuffer_getCharAt(cbuf, i) == tokens[j])
			{
				new_tail++;
				anyToken = 1;
				break;
			}
		if (anyToken == 0)
			break;
	}
	cbuf->tail = new_tail % cbuf->bufferMaxLength;

	return cbuf->ret;
}

/**
 * @brief CircBuffer_getLine Returns pointer to null-terminated string without (CR)LF at the end. All CR's which aren't followed with LF will be preserved
 * @param cbuf Pointer to circular buffer struct
 * @return Pointer to null-terminated string. NULL if cbuf is invalid. If no line found - returns empty string
 */
char* CircBuffer_getLine(CircBuffer_HandleTypeDef *cbuf)
{
    if(cbuf->newlineCount>=0)
    {
        if(CircBuffer_getLineCount(cbuf)<=0)
        {
            CircBuffer_prepareRetMemory(cbuf, 0);
            cbuf->ret[0]='\0';
            return cbuf->ret;
        }
        int cnt = CircBuffer_getCharIndex(cbuf, '\n');
        CircBuffer_prepareRetMemory(cbuf, cnt);
        for (int i = 0; i <= cnt; ++i)
            cbuf->ret[i]=CircBuffer_getChar(cbuf);
        cbuf->ret[cnt]='\0';
        return cbuf->ret;
    }

    //non-optimised
    char* ret = CircBuffer_getLineTokenized(cbuf,"\n");
    if(ret[strlen(ret)-1]=='\r')
       ret[strlen(ret)-1]='\0';
    return ret;
}

/**
 * @brief CircBuffer_getLineCount Returns ammount of LF terminated lines found in buffer.
 * @param cbuf Pointer to circular buffer struct
 * @return ammount of LF's found in buffer
 */
int CircBuffer_getLineCount(CircBuffer_HandleTypeDef *cbuf)
{
    if(cbuf->newlineCount>=0)
        return cbuf->newlineCount;

    //non-optimised
    int cnt = 0;
    for (int i = 0; i < CircBuffer_getCount(cbuf); ++i)
        if(CircBuffer_getCharAt(cbuf,i)=='\n')
            cnt++;

    return cnt;
}

/**
 * @brief CircBuffer_addChar Appends char to buffer if possible, that means if head won't move forward onto tail.
 * @param cbuf Pointer to circular buffer struct
 * @param chr Char to be appended
 * @return 0 if operation failed, otherwise 1
 */
uint8_t CircBuffer_addChar(CircBuffer_HandleTypeDef *cbuf, char chr)
{
    if (cbuf == NULL || cbuf->buffer == NULL)
		return 0;

    //if optimiseNewline was set and previous char was CR, replace it
    if(cbuf->newlineCount>=0 && chr=='\n' && cbuf->buffer[(cbuf->head-1)]=='\r')
    {
        cbuf->buffer[cbuf->head-1]= '\n';
        cbuf->newlineCount++;
        return 1;
    }


    //check if buffer has space for new char
    if (CircBuffer_getCount(cbuf) == cbuf->bufferMaxLength - 1)
		return 0;

    //add char
	cbuf->buffer[cbuf->head] = chr;
	if (++cbuf->head >= cbuf->bufferMaxLength)
		cbuf->head = 0;

	//update newline count
	if(cbuf->newlineCount>=0 && chr=='\n')
            cbuf->newlineCount++;

	return 1;
}
