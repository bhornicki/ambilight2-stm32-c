/*
 * UsbParser.c
 *
 *  Created on: 8 gru 2019
 *      Author: HHV
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include "Commander.h"
#include "usb_device.h"
#include "usbd_cdc_if.h"
#include "CircularBuffer.h"
#include "ws2812b.h"

static int LOST_CONNECTION_TIME = -1; //ms
static int LAST_CONNECTION_TIME = -1;
static int LED_COUNT;
static uint8_t CORRECT_GAMMA = 0;

#define ENDLINE				"\n"
#define CMD_SEND_LINE_HEX	"sethex"
#define CMD_SET_ALL_HEX		"sendhex"
#define CMD_SET_LED_COUNT	"ledcnt"
#define CMD_SET_TIMEOUT		"timeout"
#define CMD_SET_GAMMA		"gammacorrection"
#define CMD_GET_NAME		"whoami"
#define MCU_ID_PREFIX 		"HHV AMBILIGHT2 STM32-"
#define RESP_OK				"ok\n"						//Oh C lang, why does simple "ok" + ENDLINE not work?
#define RESP_ERR_PREFIX		"error "


uint32_t hex2int_nextPtr(char **hexPtr)
{
	char *hex = *hexPtr;
	uint32_t val = 0;
	uint8_t byte;
	//skip spaces and 0x prefix
	while (1)
	{
		if (*hex == ' ')
			hex++;
		else if (*hex == '0' && (*(hex + 1) == 'X' || *(hex + 1) == 'x'))
			hex += 2;
		else
			break;
	}

	while ((byte = *hex++))
	{
		// transform hex character to the 4bit equivalent number, using the ascii table indexes
		if (byte >= '0' && byte <= '9')
			byte = byte - '0';
		else if (byte >= 'a' && byte <= 'f')
			byte = byte - 'a' + 10;
		else if (byte >= 'A' && byte <= 'F')
			byte = byte - 'A' + 10;
		else
			break;	//finish conversion when char does not match.

		// shift 4 to make space for new digit, and add the 4 bits of the new digit
		val = (val << 4) | (byte & 0xF);
	}

	*hexPtr=hex-1;
	return val;
}

uint32_t hex2int(char *hex)
{
	return hex2int_nextPtr(&hex);
}

void sendError(char * msg)
{
	CDC_Transmit_String_FS(RESP_ERR_PREFIX);
	CDC_Transmit_String_FS(msg);
	CDC_Transmit_String_FS(ENDLINE);
}

void Commander_init(SPI_HandleTypeDef *spi_handle)
{
	WS2812B_SetHspi(spi_handle);

	//TODO: save & load defaults
	if (LED_COUNT > 0)
		WS2812B_Resize(LED_COUNT);
}

void Commander_parseMessage(char *str)
{
	int len =  strlen(str);
	if (str == NULL || len == 0)
		return;

	for (int i = 0; i < len; ++i)
		str[i] = tolower(str[i]);

	if (strstr(str, CMD_GET_NAME) == str)	//check if message starts with "whoami"
	{
		//send back unique ID
		const uint32_t uid[3] =
		{ HAL_GetUIDw0(), HAL_GetUIDw1(), HAL_GetUIDw2() };

		char msg[64] = MCU_ID_PREFIX;
		for (int i = 0; i < 3; ++i)
		{
			itoa(uid[i], msg + strlen(msg), 16);
		}
		strcpy(msg+strlen(msg), ENDLINE);
		CDC_Transmit_String_FS(msg);

	}
	else if (strstr(str, CMD_SET_LED_COUNT) == str)
	{
		int led_count = atoi(str + strlen(CMD_SET_LED_COUNT));
		if(led_count<0)
			sendError("invalid led count");

		LED_COUNT=led_count;
		WS2812B_Resize(LED_COUNT);
		CDC_Transmit_String_FS(RESP_OK);
	}
	else if (strstr(str, CMD_SEND_LINE_HEX) == str)
	{
		//format: 0xab,0xcd,0xef;
		uint8_t isErr = 0;
		int r, g, b;
		char *ptr = str + strlen(CMD_SEND_LINE_HEX);
		for (int i = 0; i < LED_COUNT; ++i)
		{
			r=hex2int_nextPtr(&ptr);
			if((isErr=*ptr++!=','))
				break;
			g=hex2int_nextPtr(&ptr);
			if((isErr=*ptr++!=','))
				break;
			b=hex2int_nextPtr(&ptr);
			if((isErr=*ptr++!=';'))
				break;

			if(CORRECT_GAMMA)
			{
				r=WS2812B_gammaTable[r];
				g=WS2812B_gammaTable[g];
				b=WS2812B_gammaTable[b];
			}
			WS2812B_SetDiodeRGB(i, (uint8_t) r, (uint8_t) g, (uint8_t) b);
		}
		if(isErr)
			sendError("not enough data received");
		else
			CDC_Transmit_String_FS(RESP_OK);
		WS2812B_Refresh();
	}
	else if (strstr(str, CMD_SET_ALL_HEX) == str)
	{
		uint8_t c[3];
		char *val = (str + strlen(CMD_SET_ALL_HEX));

		for (uint8_t i = 0; i < 3; ++i)
		{
			c[i] = (uint8_t) hex2int_nextPtr(&val);
			if(CORRECT_GAMMA)
				c[i]=WS2812B_gammaTable[c[i]];
			//check if next char after hex is null termination or any char followed by null termination,
			if(*val==0 && *(val+1)==0 && i<2)
			{
				sendError("not enough data");
				return;
			}
			//skip comma character
			++val;

		}
		WS2812B_SetAllDiodesRGB(c[0],c[1],c[2]);
		WS2812B_Refresh();

		CDC_Transmit_String_FS(RESP_OK);
	}
	else if (strstr(str, CMD_SET_TIMEOUT) == str)
	{
		LOST_CONNECTION_TIME = atoi(str + strlen(CMD_SET_TIMEOUT));
		if (LOST_CONNECTION_TIME <= 0)
			LOST_CONNECTION_TIME = -1;
		CDC_Transmit_String_FS(RESP_OK);
	}
	else if (strstr(str, CMD_SET_GAMMA) == str)
	{
		char *c = (str + strlen(CMD_SET_ALL_HEX));
		//ignore spaces
		while(*c==' ' || *c=='\t')
			++c;
		//single-letter values
		if((*c=='1'|| *c=='t') && *(c+1)==0)
			CORRECT_GAMMA=1;
		else if((*c=='0'|| *c=='f') && *(c+1)==0)
			CORRECT_GAMMA=0;
		//true or false string
		else if(strstr(c,"true")==c)
			CORRECT_GAMMA=1;
		else if(strstr(c,"false")==c)
			CORRECT_GAMMA=0;
		else
		{
			sendError("invalid bool value");
			return;
		}
		CDC_Transmit_String_FS(RESP_OK);
	}

	else
	{
		sendError("unrecognized command");
	}

}

void Commander_mainLoop()
{
	if (LAST_CONNECTION_TIME == 0)
	{
		memset(WS2812B_GetPixels(), 0, LED_COUNT * sizeof(Ws2812b_color));
		WS2812B_Refresh();
		//TODO: inform user about lost connection and reset defaults
	}
	if (CircBuffer_getLineCount(CDC_Get_RX_Buffer()))
	{
		Commander_parseMessage(CircBuffer_getLine(CDC_Get_RX_Buffer()));
		LAST_CONNECTION_TIME = LOST_CONNECTION_TIME;
	}
}

void HAL_SYSTICK_Callback()
{
	if (LAST_CONNECTION_TIME > 0)
		--LAST_CONNECTION_TIME;
}
