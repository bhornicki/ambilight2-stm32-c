/*
 * ws2812b.c
 *
 *	The MIT License.
 *	Created on: 14.07.2017
 *		Author: Mateusz Salamon
 *		www.msalamon.pl
 *		mateusz@msalamon.pl
 */

#include "stm32f1xx_hal.h"
#include "main.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>

#include "ws2812b.h"

//user "accessible"
static SPI_HandleTypeDef *WS2812B_hspi;
static Ws2812b_color *WS2812B_ledArray;
static int WS2812B_ledCount;

//Used for DMA control
#define DMA_BUF_SIZE 48
static const uint8_t DMA_SIG_ZERO = 0b11000000;
static const uint8_t DMA_SIG_ONE = 0b11111000;
static const uint8_t DMA_RESET_COUNT = 2;

static uint8_t dmaBuffer[DMA_BUF_SIZE];
static int dmaCurrentLed;
static uint8_t dmaResetSignal;

//private function prototypes
void WS2812_FillDmaBuffer(uint8_t idx);

void WS2812B_SetHspi(SPI_HandleTypeDef *spi_handle)
{
	WS2812B_hspi = spi_handle;
}
void WS2812B_Init(SPI_HandleTypeDef *spi_handle, int led_count)
{
	WS2812B_SetHspi(spi_handle);
	WS2812B_Resize(led_count);
}

void WS2812B_Deinit()
{
	free(WS2812B_ledArray);
	WS2812B_hspi = NULL;
	WS2812B_ledCount = 0;
	WS2812B_ledArray = NULL;
}

void WS2812B_Resize(int led_count)
{
	WS2812B_ledCount = led_count;
	WS2812B_ledArray = realloc(WS2812B_ledArray, sizeof(Ws2812b_color) * led_count);
}

void WS2812B_SetDiodeColor(int16_t diode_id, uint32_t color)
{
	if (WS2812B_ledArray == NULL || diode_id >= WS2812B_ledCount || diode_id < 0)
		return;
	WS2812B_ledArray[diode_id].red = ((color >> 16) & 0x000000FF);
	WS2812B_ledArray[diode_id].green = ((color >> 8) & 0x000000FF);
	WS2812B_ledArray[diode_id].blue = (color & 0x000000FF);
}

void WS2812B_SetDiodeColorStruct(int16_t diode_id, Ws2812b_color color)
{
	if (WS2812B_ledArray == NULL || diode_id >= WS2812B_ledCount || diode_id < 0)
		return;
	WS2812B_ledArray[diode_id] = color;
}

void WS2812B_SetAllDiodesRGB(uint8_t R, uint8_t G, uint8_t B)
{
	if (WS2812B_ledArray == NULL || WS2812B_ledCount < 1)
		return;

	for (int i = 0; i < WS2812B_ledCount; ++i)
		WS2812B_SetDiodeRGB(i, R, G, B);
}

void WS2812B_SetDiodeRGB(int16_t diode_id, uint8_t R, uint8_t G, uint8_t B)
{
	if (WS2812B_ledArray == NULL || diode_id >= WS2812B_ledCount || diode_id < 0)
		return;
	WS2812B_ledArray[diode_id].red = R;
	WS2812B_ledArray[diode_id].green = G;
	WS2812B_ledArray[diode_id].blue = B;
}

uint32_t WS2812B_GetColor(int16_t diode_id)
{
	if (WS2812B_ledArray == NULL || diode_id >= WS2812B_ledCount || diode_id < 0)
		return 0;
	uint32_t color = 0;
	color |= ((WS2812B_ledArray[diode_id].red & 0xFF) << 16);
	color |= ((WS2812B_ledArray[diode_id].green & 0xFF) << 8);
	color |= (WS2812B_ledArray[diode_id].blue & 0xFF);
	return color;
}

uint8_t* WS2812B_GetPixels(void)
{
	return (uint8_t*) WS2812B_ledArray;
}

//
//	Set diode with HSV model
//
//	Hue 0-359
//	Saturation 0-255
//	Brightness(Value) 0-255
//
void WS2812B_SetDiodeHSV(int16_t diode_id, uint16_t Hue, uint8_t Saturation, uint8_t Brightness)
{
	if (WS2812B_ledArray == NULL || diode_id >= WS2812B_ledCount || diode_id < 0)
		return;
	uint16_t Sector, Fracts, p, q, t;

	if (Saturation == 0)
	{
		WS2812B_ledArray[diode_id].red = Brightness;
		WS2812B_ledArray[diode_id].green = Brightness;
		WS2812B_ledArray[diode_id].blue = Brightness;
	}
	else
	{
		if (Hue >= 360)
			Hue = 359;

		Sector = Hue / 60; // Sector 0 to 5
		Fracts = Hue % 60;
		p = (Brightness * (255 - Saturation)) / 256;
		q = (Brightness * (255 - (Saturation * Fracts) / 360)) / 256;
		t = (Brightness * (255 - (Saturation * (360 - Fracts)) / 360)) / 256;

		switch (Sector)
		{
			case 0:
				WS2812B_ledArray[diode_id].red = Brightness;
				WS2812B_ledArray[diode_id].green = (uint8_t) t;
				WS2812B_ledArray[diode_id].blue = (uint8_t) p;
				break;
			case 1:
				WS2812B_ledArray[diode_id].red = (uint8_t) q;
				WS2812B_ledArray[diode_id].green = Brightness;
				WS2812B_ledArray[diode_id].blue = (uint8_t) p;
				break;
			case 2:
				WS2812B_ledArray[diode_id].red = (uint8_t) p;
				WS2812B_ledArray[diode_id].green = Brightness;
				WS2812B_ledArray[diode_id].blue = (uint8_t) t;
				break;
			case 3:
				WS2812B_ledArray[diode_id].red = (uint8_t) p;
				WS2812B_ledArray[diode_id].green = (uint8_t) q;
				WS2812B_ledArray[diode_id].blue = Brightness;
				break;
			case 4:
				WS2812B_ledArray[diode_id].red = (uint8_t) t;
				WS2812B_ledArray[diode_id].green = (uint8_t) p;
				WS2812B_ledArray[diode_id].blue = Brightness;
				break;
			default:		// case 5:
				WS2812B_ledArray[diode_id].red = Brightness;
				WS2812B_ledArray[diode_id].green = (uint8_t) p;
				WS2812B_ledArray[diode_id].blue = (uint8_t) q;
				break;
		}
	}
}

int WS2812B_GetLedCount()
{
	return WS2812B_ledCount;
}
void WS2812B_Refresh()
{
	if (WS2812B_ledCount < 1) //|| WS2812B_hspi ==NULL)
		return;

	dmaCurrentLed = 0;
	dmaResetSignal = DMA_RESET_COUNT;

	memset(dmaBuffer, 0, DMA_BUF_SIZE);
	HAL_SPI_Transmit_DMA(WS2812B_hspi, dmaBuffer, DMA_BUF_SIZE); // Additional 3 for reset signal
	while (HAL_DMA_STATE_READY != HAL_DMA_GetState(WS2812B_hspi->hdmatx));
	HAL_GPIO_WritePin(GPIOB,DEBUG_Pin, GPIO_PIN_RESET);
}

void WS2812_FillDmaBuffer(uint8_t idx)
{
	if (dmaResetSignal)
	{
		--dmaResetSignal;
		return;
	}
	if (dmaCurrentLed > WS2812B_ledCount)
	{
		HAL_SPI_DMAStop(WS2812B_hspi);
		return;
	}

	for (int8_t k = 7; k >= 0; --k)
	{
		dmaBuffer[idx] = (WS2812B_ledArray[dmaCurrentLed].green & (1 << k)) ? DMA_SIG_ONE : DMA_SIG_ZERO;
		dmaBuffer[idx + 8] = (WS2812B_ledArray[dmaCurrentLed].red & (1 << k)) ? DMA_SIG_ONE : DMA_SIG_ZERO;
		dmaBuffer[idx + 16] = (WS2812B_ledArray[dmaCurrentLed].blue & (1 << k)) ? DMA_SIG_ONE : DMA_SIG_ZERO;
		++idx;
	}
	dmaCurrentLed++;
}

void HAL_SPI_TxHalfCpltCallback(SPI_HandleTypeDef *hspi)
{
	if (hspi == WS2812B_hspi)
		WS2812_FillDmaBuffer(0);
}

void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi)
{
	if (hspi == WS2812B_hspi)
		WS2812_FillDmaBuffer(DMA_BUF_SIZE / 2);
}


const uint8_t WS2812B_gammaTable[256] = {
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,  1,  1,  1,  1,
    1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,  2,  3,  3,  3,  3,
    3,  3,  4,  4,  4,  4,  5,  5,  5,  5,  5,  6,  6,  6,  6,  7,
    7,  7,  8,  8,  8,  9,  9,  9, 10, 10, 10, 11, 11, 11, 12, 12,
   13, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19, 19, 20,
   20, 21, 21, 22, 22, 23, 24, 24, 25, 25, 26, 27, 27, 28, 29, 29,
   30, 31, 31, 32, 33, 34, 34, 35, 36, 37, 38, 38, 39, 40, 41, 42,
   42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57,
   58, 59, 60, 61, 62, 63, 64, 65, 66, 68, 69, 70, 71, 72, 73, 75,
   76, 77, 78, 80, 81, 82, 84, 85, 86, 88, 89, 90, 92, 93, 94, 96,
   97, 99,100,102,103,105,106,108,109,111,112,114,115,117,119,120,
  122,124,125,127,129,130,132,134,136,137,139,141,143,145,146,148,
  150,152,154,156,158,160,162,164,166,168,170,172,174,176,178,180,
  182,184,186,188,191,193,195,197,199,202,204,206,209,211,213,215,
  218,220,223,225,227,230,232,235,237,240,242,245,247,250,252,255
};

